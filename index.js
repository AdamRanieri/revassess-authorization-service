import express from "express";
import cors from "cors";
import {Datastore} from "@google-cloud/datastore";
import encrypt from "./encryption.js"
import decrypt from "./decryption.js";
import jwt from "jsonwebtoken";

const app = express();
const datastore = new Datastore();
const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(cors());

/**
 * Check for valid email. If valid then check password.
 * If password is valid then respond with a JWT.
 */
app.patch("/login", async (req, res) =>
{
    const email = req.body["email"];
    const password = req.body["password"];

    const query = datastore.createQuery("Trainers").filter("email", "=", email);
    const response = await datastore.runQuery(query);

    if (response[0].length > 0) // email is valid, check for valid password.
    {
        if (decrypt(response[0][0]["password"]) === password)
        {
            jwt.sign({email: email}, password, (err, token) =>
            {
                res.status(200).send({"token": token});
            });
            return;
        }
    }
    
    res.status(401).send(false);
    return;
});

app.post("/create", async (req, res) =>
{
    // take in an email and a password.
    const email = req.body["email"];
    const password = encrypt(req.body["password"]);

    const query = datastore.createQuery("Trainers").filter("email", "=", email);
    const response = await datastore.runQuery(query);

     // If the email already exists in the database, do not allow a new account for that email.
    if (response[0].length > 0)
    {
        res.status(403).send(false);
        return;
    }

    // store the new account in the datastore database.
    const entity =
    {
        key: datastore.key("Trainers"),
        data: {email: email, password: password}
    };

    await datastore.insert(entity);
    res.status(201).send(true);
    return;
});

app.listen(PORT, ()=>console.log("Application started."));