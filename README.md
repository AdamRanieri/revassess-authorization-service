# RevAssess-Authorization-Service

## Service Description
This service will validate login attempts for the Revassess project. Authorized users may also create new accounts for trainers.

## Technologies Used
- NodeJS - Version 13
- Express - Version 4.17.1
- Cors - Version 2.8.5
- jsonwebtoken - Version 8.5.1
- @google-cloud/datastore - Version 6.5.0

## Features
- Login with existing credentials.
- Create new account.
- Encrypt passwords when a new account is created.
- Passwords are decrypted, only in the code, to verify that the user entered a valid password.
- Only one account may be created per email address.

## Getting Started
#### Manual install and run locally.
```bash
git clone https://gitlab.com/Thomas_Smithey/town-meeting-project.git
```
- You will need to clone all repos from the [Revassess](https://gitlab.com/AdamRanieri/revassess-frontend) project.
- A Datastore database is needed to store Trainer account credentials.
- If you haven't already, [install NodeJS](https://nodejs.org/en/download/).
- Run 'npm install' on each directory.
- Open a terminal in each directory and type "npm start" then press enter.

## Usage
- Assuming my group's services are deployed on GCP and our frontend is set up with Firebase, I will include a link here.
- All login attempts, or account creations, will then be processed by the Authorization Service.

## Contributors
- Thomas Smithey
- Alan Tillmann
- Donavan Merrin